package com.example.basicapplication

import com.google.gson.annotations.SerializedName

data class TrivialResponse(var response_code: String, var results: List<Trivial>){

}
