package com.example.basicapplication

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.basicapplication.databinding.FragmentSecondBinding

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */

enum class CalculatorState(val state: Int) {
    INIT(0),
    FIRST_FIGURE(1),
    SECOND_FIGURE(2),
    RESULT(3);
}


class SecondFragment : Fragment() {

    private var _binding: FragmentSecondBinding? = null
    private var state = CalculatorState.INIT
    private var firstFigure: Float = 0F
    private var secondFigure: Float = 0F
    private var result: Float = 0F
    private var operator = ""


    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentSecondBinding.inflate(inflater, container, false)
        return binding.root

    }

    fun handleNumber(number: Int){
        when (state) {
            CalculatorState.INIT -> {
                firstFigure = number.toFloat();
                state = CalculatorState.FIRST_FIGURE
                binding.display.setText(binding.display.text.toString() + number.toString())
            }
            CalculatorState.FIRST_FIGURE -> {
                firstFigure = 10 * firstFigure + number.toFloat();
                binding.display.setText(binding.display.text.toString() + number.toString())
            }
            CalculatorState.SECOND_FIGURE -> {
                secondFigure = 10 * secondFigure + number.toFloat();
                binding.display.setText(binding.display.text.toString() + number.toString())
            }
            CalculatorState.RESULT -> {
                secondFigure = 0F
                result = 0F
                operator = ""
                firstFigure = number.toFloat()
                state = CalculatorState.FIRST_FIGURE
                binding.display.setText(number.toString())
            }
            else -> print("Invalid Direction")
        }

    }

    fun resolve(): Float {
        when (operator){
            "+" -> {return firstFigure + secondFigure}
            "-" -> {return firstFigure - secondFigure}
            "x" -> {return firstFigure * secondFigure}
            "/" -> {return firstFigure / secondFigure}
        }
        return -1F
    }

    fun handleSymbol(symbol: String) {
        when (state) {
            CalculatorState.INIT -> {
            }
            CalculatorState.FIRST_FIGURE -> {
                if(symbol == "+" || symbol == "-" || symbol == "x" || symbol == "/") {
                    operator = symbol
                    state = CalculatorState.SECOND_FIGURE
                    binding.display.setText(binding.display.text.toString() + symbol)
                }
            }
            CalculatorState.SECOND_FIGURE -> {
                if(symbol == "="){
                    result = resolve()
                    state = CalculatorState.RESULT
                    binding.display.setText(binding.display.text.toString() + symbol + result.toString())
                }
            }
            CalculatorState.RESULT -> {
                if(symbol == "+" || symbol == "-" || symbol == "x" || symbol == "/") {
                    firstFigure = result
                    operator = symbol
                    secondFigure = 0F
                    result = 0F
                    state = CalculatorState.SECOND_FIGURE
                    binding.display.setText(firstFigure.toString() + symbol)

                }
            }
            else -> print("Invalid Direction")
        }

    }
    fun handleButton(value: Any){
        if(value is Int) {
            handleNumber(value as Int)
        }else if(value is String) {
            handleSymbol(value as String)
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonCero.setOnClickListener {
            handleButton(0)
        }
        binding.button1.setOnClickListener {
            handleButton(1)
        }
        binding.button2.setOnClickListener {
            handleButton(2)
        }
        binding.button3.setOnClickListener {
            handleButton(3)
        }
        binding.button4.setOnClickListener {
            handleButton(4)
        }
        binding.button5.setOnClickListener {
            handleButton(5)
        }
        binding.button6.setOnClickListener {
            handleButton(6)
        }
        binding.button7.setOnClickListener {
            handleButton(7)
        }
        binding.button8.setOnClickListener {
            handleButton(8)
        }
        binding.button9.setOnClickListener {
            handleButton(9)
        }
        binding.buttonPlus.setOnClickListener {
            handleButton("+")
        }
        binding.buttonMinus.setOnClickListener {
            handleButton("-")
        }
        binding.buttonMult.setOnClickListener {
            handleButton("x")
        }
        binding.buttonDiv.setOnClickListener {
            handleButton("/")
        }
        binding.buttonEqual.setOnClickListener {
            handleButton("=")
        }
        binding.buttonPoint.setOnClickListener {
            handleButton(".")
        }


//        binding.buttonSecond.setOnClickListener {
//            findNavController().navigate(R.id.action_SecondFragment_to_FirstFragment)
//        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}