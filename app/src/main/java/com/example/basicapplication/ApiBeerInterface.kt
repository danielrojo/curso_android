package com.example.basicapplication

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface ApiBeerInterface {
    @GET("beers")
    fun getBeers() : Call<List<Beer>>
    companion object {
        var BASE_URL = "https://api.punkapi.com/v2/"
        fun create() : ApiBeerInterface {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
            return retrofit.create(ApiBeerInterface::class.java)
        }
    }

}
