package com.example.basicapplication

import com.google.gson.annotations.SerializedName

data class Trivial(var question: String, var correct_answer:String, var incorrect_answers: List<String>){

    fun getAnswers(): List<String> {
        var result = mutableListOf<String>()
        result = this.incorrect_answers as MutableList<String>
        result.add(this.correct_answer)
        return result
    }

    fun getRightAnswer(): String {
        return this.correct_answer
    }

}
