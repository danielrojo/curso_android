package com.example.basicapplication

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface ApiApodInterface {
    @GET("apod?api_key=DEMO_KEY")
    fun getApod() : Call<Apod>
    companion object {
        var BASE_URL = "https://api.nasa.gov/planetary/"
        fun create() : ApiApodInterface {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
            return retrofit.create(ApiApodInterface::class.java)
        }
    }

}
