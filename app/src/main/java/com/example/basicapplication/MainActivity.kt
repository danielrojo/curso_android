package com.example.basicapplication

import android.os.Bundle
import android.util.Log
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import android.view.Menu
import android.view.MenuItem
import com.example.basicapplication.databinding.ActivityMainBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding

    val apiInterface = ApiApodInterface.create().getApod()
    open lateinit var apod: Apod

    val apiTrivialInterface = ApiTrivialInterface.create().getTrivial()
    open lateinit var trivial: TrivialResponse

    val apiBeersInterface = ApiBeerInterface.create().getBeers()
    open lateinit var beers: List<Beer>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)

        val navController = findNavController(R.id.nav_host_fragment_content_main)
        appBarConfiguration = AppBarConfiguration(navController.graph)
        setupActionBarWithNavController(navController, appBarConfiguration)

        binding.fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }
    }

    override fun onResume() {
        super.onResume()
        apiInterface.enqueue( object : Callback<Apod> {
            override fun onResponse(call: Call<Apod>?, response: Response<Apod>?) {
                if(response?.body() != null){
                    apod = response.body()!!
                    Log.i("DATA",apod.toString())

                }
            }
            override fun onFailure(call: Call<Apod>?, t: Throwable?) {
                Log.e("DATA",t.toString())
            }
        })
        apiTrivialInterface.enqueue( object : Callback<TrivialResponse> {
            override fun onResponse(call: Call<TrivialResponse>?, response: Response<TrivialResponse>?) {
                if(response?.body() != null){
                    trivial = response.body()!!
                    Log.i("DATA",trivial.toString())

                }
            }
            override fun onFailure(call: Call<TrivialResponse>?, t: Throwable?) {
                Log.e("DATA",t.toString())
            }
        })
        apiBeersInterface.enqueue(object : Callback<List<Beer>> {
            override fun onResponse(call: Call<List<Beer>>?, response: Response<List<Beer>>?) {
                if(response?.body() != null){
                    beers = response.body()!!
                    Log.i("DATA",beers.toString())

                }
            }
            override fun onFailure(call: Call<List<Beer>>, t: Throwable) {
                TODO("Not yet implemented")
                Log.i("DATA",t.toString())
            }
        })
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        return navController.navigateUp(appBarConfiguration)
                || super.onSupportNavigateUp()
    }
}