package com.example.basicapplication

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface ApiTrivialInterface {
    @GET("api.php?amount=10&type=multiple")
    fun getTrivial() : Call<TrivialResponse>
    companion object {
        var BASE_URL = "https://opentdb.com/"
        fun create() : ApiTrivialInterface {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
            return retrofit.create(ApiTrivialInterface::class.java)
        }
    }

}
