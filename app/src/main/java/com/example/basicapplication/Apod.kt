package com.example.basicapplication

import com.google.gson.annotations.SerializedName

data class Apod(var title: String, var explanation: String, var url: String, var date: String){

}