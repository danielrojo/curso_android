package com.example.basicapplication

import android.util.Log
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView

import com.example.basicapplication.placeholder.PlaceholderContent.PlaceholderItem
import com.example.basicapplication.databinding.FragmentTrivialCardBinding

/**
 * [RecyclerView.Adapter] that can display a [PlaceholderItem].
 * TODO: Replace the implementation with code for your data type.
 */
class MyTrivialCardRecyclerViewAdapter(
    private val values: List<Trivial>
) : RecyclerView.Adapter<MyTrivialCardRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            FragmentTrivialCardBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.idView.text = item.question
        holder.button0.text = item.getAnswers()[0]
        holder.button1.text = item.getAnswers()[1]
        holder.button2.text = item.getAnswers()[2]
        holder.button3.text = item.getAnswers()[3]

    //        holder.contentView.text = item.getAnswers().toString()
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(binding: FragmentTrivialCardBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val idView: TextView = binding.itemNumber
        val button0: Button = binding.button0
        val button1: Button = binding.button1
        val button2: Button = binding.button2
        val button3: Button = binding.button3

        override fun toString(): String {
            return super.toString() + " '" + idView.text + "'"
        }
    }

}